const NodeGeocoder = require('node-geocoder');

const options = {
    provider: process.env.GEOCODER_PROVIDER,
    httpAdapter: 'https',
    apiKey: null,
    formatter: null
};

const geocoder = NodeGeocoder(options);

// Using callback
geocoder.geocode('addresse', function(err, res) {
    console.log(res)
})