const nodemailer = require('nodemailer')

module.exports = (to, token, operation) => {
    let message = "";
    if (operation === 'registration') message = `In order to activate your account click on the link <a href="http://localhost:3001/activate/${token}">HERE</a>`
    if (operation === 'password') message = `To change the password click <a href="http://localhost:3001/recover-password/${token}">HERE</a>`
    const transporter = nodemailer.createTransport({
    service: 'gmail',
    host: 'smtp.gmail.com',
    secure: false,
    auth: {
      user: process.env.EMAIL,
      pass: process.env.PASSWORD
    }
  });

  let mailOptions = {
    from: process.env.EMAIL,
    to: to,
    subject: 'SportApp registration comfirmation',
    text: message
  };

  transporter.sendMail(mailOptions, function(error, info){
    if (error) {
      console.log(error);
    } else {
      console.log('Email sent: ' + info.response);
    }
  })
}

 