const jwt = require('jsonwebtoken');

module.exports = async (req, res, next) =>  {
    try { 
        const token = req.cookies.token || req.query.token;
        const decodedToken = await jwt.verify(token, process.env.SECRET);

        if (decodedToken) {
            req.body.decodedToken = decodedToken;
            req.body.token = token;
            next()
        }
    } catch(err) {
        res.sendStatus(404);
    }
}