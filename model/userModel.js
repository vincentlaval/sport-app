const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const saltRounds = 10;

const userSchema = new mongoose.Schema(
  {
    fname: {
      type: String,
      required: [true, 'Please provide your first name.'],
      trim: true
    },
    lname: {
      type: String,
      required: [true, 'Please provide your last name.'],
      trim: true
    },
    gender: {
      type: String,
      enum: ['man', 'woman']
    },
    email: {
      type: String,
      unique: true,
      required: [true, 'Please provide an email'],
      trim: true
    },
    photo: {
      type: String
    },
    description: {
      type: String,
      maxlength: 50,
      trim: true
    },
    password: {
      type: String,
      required: [true, 'Please provide your password'],
      trim: true
    },
    status: {
      //TODO
    },
    friends: [{ type: mongoose.Schema.Types.ObjectId, ref: 'user' }],
    isVerified: {
      type: Boolean,
      default: false
    },
    createdEvents: [{ type: mongoose.Schema.Types.ObjectId, ref: 'event' }],
    inEvents: [{ type: mongoose.Schema.Types.ObjectId, ref: 'event' }]
  },
  { timestamps: true }
);

userSchema.pre('save', function(next) {
  let user = this;
  bcrypt.hash(user.password, saltRounds, function(err, hash) {
    if (err) return next(err);
    user.password = hash;
    next();
  });
});

module.exports = mongoose.model('user', userSchema);
