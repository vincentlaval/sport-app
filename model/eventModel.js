const mongoose = require('mongoose');

const eventSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: [true, 'Please provide a name'],
      trim: true
    },
    sport: {
      type: String,
      enum: ['Foot', 'Basket', 'Tennis', 'Golf', 'Volley', 'Running'],
      trim: true,
      required: [true, 'Please provide a sport name']
    },
    description: {
      type: String,
      maxlength: 150,
      minlength: 20,
      trim: true
    },
    date: {
      type: Date,
      required: [true, 'Please provide a date']
    },
    location: {
      type: {
        type: String,
        enum: ['Point']
        // required: true
      },
      coordinates: {
        type: [Number] // [longitude, latitude]
        // required: true
      }
    },
    participants: [{ type: [mongoose.Schema.Types.ObjectId], ref: 'user' }],
    creator: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'user',
      required: [true, 'Creator must be provided']
    }
  },
  { timestamps: true }
);


module.exports = mongoose.model('event', eventSchema);
