const router = require('express').Router();

const {
  getEvent,
  getAllEvents,
  createEvent,
  updateEvent,
  deleteEvent
} = require('../controllers/eventControllers');

router
  .route('/')
  .get(getAllEvents)
  .post(createEvent);

router
  .route('/:id')
  .get(getEvent)
  .put(updateEvent)
  .delete(deleteEvent);

module.exports = router;
