const express = require('express');
const morgan = require('morgan');
const helmet = require('helmet');
const cookieParser = require('cookie-parser');
const cors = require('cors');

const {
  onLogin,
  activateUser,
  userSignout,
  forgotPassword,
  changePassword,
  getFriends,
  updateUser,
  getUser,
  updateUserDetailes,
  getUserid
} = require('./controllers/userControllers');
const { addUser } = require('./controllers/userControllers');
const app = express();
const jwtmiddleware = require('./middleware/jwt');
const eventRoutes = require('./routes/eventRoutes');
const userRoutes = require('./routes/userRoutes');

// app.use(cors({ origin: 'http://localhost:3000', credentials: true }));
app.use(cookieParser());
app.use(helmet());
app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static('public'));

app.use('/api/users', userRoutes);
app.post('/api/user/:id', jwtmiddleware, addUser);
app.use('/api/events', eventRoutes);
app.post('/api/login', onLogin);
app.post('/api/activate', jwtmiddleware, activateUser);
app.post('/api/signout', jwtmiddleware, userSignout);
app.post('/api/auth', jwtmiddleware, async (req, res) => {
  res.sendStatus(200);
});

app.post('/api/friends', jwtmiddleware, getFriends);
app.post('/api/password', forgotPassword);
app.put('/api/password', jwtmiddleware, updateUser);
app.post('/api/profile', jwtmiddleware, getUser);

app.post('/api/userid', jwtmiddleware, getUserid);

module.exports = app;
