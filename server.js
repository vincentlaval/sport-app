require('dotenv').config({path: './config/.env'});
require('./db/db')(process.env.MONGO_URI);
require('colors');
const server = require('./app');


const PORT = process.env.PORT;
server.listen(PORT, () => {
    console.log(`Server was connected on port ${PORT}`)
});

process.on('unhandledRejection', (err) => {
    console.log(err)
})