import React from 'react';
import { Message, Icon } from 'semantic-ui-react'

const MessageWaiting = () => (
  <Message icon color="yellow">
    <Icon name='circle notched' loading />
    <Message.Content>
      <Message.Header>Almost there!</Message.Header>
      We are in process of activating your account, it won't take long.
    </Message.Content>
  </Message>
)

export default MessageWaiting;