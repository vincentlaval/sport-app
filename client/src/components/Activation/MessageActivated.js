import React from 'react'
import { Message } from 'semantic-ui-react'

const MessageExampleIconProp = () => (
  <Message
    color="green"
    size='huge'
    icon='checkmark'
    header='Your account has been activated!'
    content='You will be redirected to login page in 5 seconds'
  />
)

export default MessageExampleIconProp