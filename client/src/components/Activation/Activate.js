import React, { useState, useEffect } from 'react'
import MessageWaiting from './MessageWaiting';
import MessageActivated from './MessageActivated';

import axios from 'axios';

function Activate(props) {
    const [isActivated, setIsActivated] = useState(false);
    useEffect(() => {
        axios.post(`http://localhost:3001/activate?token=${props.match.params.id}`)
        .then(() => {
            setIsActivated(true);
            setTimeout(()=> props.history.push('/login'), 5000)
        });
    }, [])
    return (
        <div>
            {isActivated ? <MessageActivated /> : <MessageWaiting />}
        </div>
    )
}

export default Activate;
