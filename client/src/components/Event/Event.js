import React, { useState, useEffect } from 'react';
import axios from 'axios';
import EventDisplay from './EventDisplay';

function Event(props) {
  const [eventState, setEventState] = useState({});

  let id = props.match.params.id;
  console.log(id);

  useEffect(() => {
    axios
      .get(
        `http://sportapp.formationfullstack.fr/api/events/${id}`,
        {
          headers: {
            'Content-type': 'application/json',
            crossDimain: true
          }
        },
        { withCredentials: true }
      )
      .then(data => {
        props.setUserState(true);
        setEventState({ ...data.data });
      })
      .catch(err => {
        console.log(err);
      });
  }, []);

  return (
    <div>
      {eventState._id ? <EventDisplay eventState={eventState} /> : null}
    </div>
  );
}
export default Event;
