import React, { useState } from 'react';
import ReactMapGL, { Marker } from 'react-map-gl';
import { Icon } from 'semantic-ui-react';

function MapEvent(props) {
  console.log(props);
  const lat = props.location.coordinates[1];
  const lon = props.location.coordinates[0];

  const [viewport, setViewport] = useState({
    latitude: lat,
    longitude: lon,
    zoom: 15,
    bearing: 0,
    pitch: 0
  });

  const MAPBOX_TOKEN =
    'pk.eyJ1IjoidmluY2UzMzEiLCJhIjoiY2s1YmRzbzNkMGJodzNsbzRudHAzYTk3NSJ9.rmP3XL-XRlvA1hw7fvkNSQ';
  return (
    <div>
      <ReactMapGL
        {...viewport}
        width="60vw"
        height="60vh"
        mapboxApiAccessToken={MAPBOX_TOKEN}
        mapStyle="mapbox://styles/vince331/ck5in4zak16i71imuq9ieyzhq"
        onViewportChange={viewport => {
          setViewport(viewport);
        }}
      >
        {props.location ? (
          <Marker
            latitude={props.location.coordinates[1]}
            longitude={props.location.coordinates[0]}
          >
            <Icon name="hand point down" size="large" color="teal" />
          </Marker>
        ) : null}
      </ReactMapGL>
    </div>
  );
}

export default MapEvent;
