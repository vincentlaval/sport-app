import React, { useState, useEffect } from 'react';
import { Divider, Table, Grid, Header, List } from 'semantic-ui-react';
import MapEvent from './MapEvent';
import { NavLink } from 'react-router-dom';
import Participate from '../Participate';
import Departicipate from '../Departicipate';
import axios from 'axios';

function EventDisplay(props) {
  const {
    name,
    description,
    participants,
    sport,
    address,
    date,
    location,
    _id
  } = props.eventState;
  const [user, setUser] = useState('');

  console.log(props);

  useEffect(() => {
    axios
      .post(
        'http://sportapp.formationfullstack.fr/api/userid',
        {
          headers: { 'Content-type': 'application/json', crossDimain: true }
        },
        { withCredentials: true }
      )
      .then(data => setUser(data.data))
      .catch(err => {
        props.setUserState(false);
      });
  });

  return (
    <div>
      <Header as="h1">
        <center>{name ? name : 'Event'}</center>
      </Header>
      <Divider horizontal>
        <Header as="h3">
          <i className="globe icon"></i>
          Location
        </Header>
      </Divider>
      <Grid centered columns="1">
        {location ? <MapEvent location={location} /> : null}
      </Grid>
      <br></br>
      <Divider horizontal>
        <Header as="h3">
          <i className="tag icon"></i>
          Description
        </Header>
      </Divider>
      {description ? <p>{description}</p> : <p>Description non renseignée</p>}
      <Divider horizontal>
        <Header as="h3">
          <i className="bar chart icon"></i>
          Event informations
        </Header>
      </Divider>
      <Table definition>
        <Table.Body>
          <Table.Row>
            <Table.Cell>
              <b>Sport</b>
            </Table.Cell>
            <Table.Cell>{sport ? sport : 'Sport non renseigné'}</Table.Cell>
          </Table.Row>
          <Table.Row>
            <Table.Cell>
              <b>Participants</b>
            </Table.Cell>
            <Table.Cell>
              <List horizontal>
                {participants[0] ? (
                  participants.map(p => (
                    <List.Item as={NavLink} to={`/user/${p._id}`}>
                      {p.fname}
                    </List.Item>
                  ))
                ) : (
                  <List.Item>Liste des participants non disponible</List.Item>
                )}
              </List>
            </Table.Cell>
          </Table.Row>
          <Table.Row>
            <Table.Cell>
              <b>Adresse</b>
            </Table.Cell>
            <Table.Cell>
              {address ? address : 'Addresse non renseignée'}
            </Table.Cell>
          </Table.Row>
          <Table.Row>
            <Table.Cell>
              <b>Date</b>
            </Table.Cell>
            <Table.Cell>{date ? date : 'Date non renseignée'}</Table.Cell>
          </Table.Row>
        </Table.Body>
      </Table>
      <Divider>
        {console.log(participants.map(participant => participant._id))}
        {participants.map(participant => participant._id).includes(user) ? (
          <Departicipate
            name={name}
            description={description}
            id={_id}
            participants={participants}
          />
        ) : (
          <Participate name={name} description={description} id={_id} />
        )}
      </Divider>
    </div>
  );
}

export default EventDisplay;
