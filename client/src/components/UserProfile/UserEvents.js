import React from 'react';
import { Item, Icon, Segment, Header,Label } from 'semantic-ui-react';

function UserEvents(props) {

    return (
      <Segment raised>
        <Item.Group divided>
          {props.events.length == 0 ? (
            <Header as="h2">No events!</Header>
          ) : (
            props.events.map(e => (
              <Item key={e._id}>
                <Icon name="basketball ball" size="huge" />
                <Item.Content>
                  <Item.Header as="a">{e.name}</Item.Header>
                  <Item.Meta>
                    <span className="cinema">{e.sport}</span>
                  </Item.Meta>
                  <Item.Description>{e.description}</Item.Description>
                  <Item.Extra>
                    <Label>
                      <Icon name="users" />
                      {e.participants.length}
                    </Label>
                  </Item.Extra>
                </Item.Content>
              </Item>
            ))
          )}
        </Item.Group>
      </Segment>
    );
}



export default UserEvents;
