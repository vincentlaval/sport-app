import React from 'react';
import { Card, Icon, Image, Button } from 'semantic-ui-react';
import axios from 'axios';

function UserInfo(props) {
  function addToFriends() {
    axios.post(
      `http://sportapp.formationfullstack.fr/api/user/${props.userState._id}`,
      {
        headers: { 'Content-type': 'application/json', crossDimain: true }
      },
      { withCredentials: true }
    );
  }

  console.log(props.userState._id);
  const {
    fname,
    lname,
    email,
    friends,
    description,
    createdAt
  } = props.userState;
  return (
    <Card color="teal">
      <Image
        src="https://react.semantic-ui.com/images/avatar/large/molly.png"
        wrapped
        ui={false}
      />
      <Card.Content>
        <Card.Header>
          {fname} {lname}
        </Card.Header>
        <Card.Meta>
          <div>
            <Icon name="mail" /> {email}
          </div>
          <span className="date">
            <Icon name="wait" />
            Joined in {new Date(createdAt).getFullYear()}
          </span>
        </Card.Meta>
        <Card.Description>
          {description ? description : <p>No description</p>}
        </Card.Description>
      </Card.Content>
      <Card.Content extra>
        <div className="ui one buttons">
          <Button basic color="green" onClick={addToFriends}>
            Add to Friends
          </Button>
        </div>
        <a>
          <Icon name="user" />
          {friends.length ? friends.length : 'No friends'}
        </a>
      </Card.Content>
    </Card>
  );
}

export default UserInfo;
