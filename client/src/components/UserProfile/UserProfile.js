import React, {useEffect, useState} from 'react';
import axios from 'axios';
import {Grid} from 'semantic-ui-react';
import UserInfo from './UserInfo';
import UserEvents from './UserEvents';

function UserProfile(props) {
    const [userState, setUserState] = useState();
    useEffect(() => {
        props.setImg('golf');
        axios.get(`http://sportapp.formationfullstack.fr/api/users/${props.match.params.id}`)
        .then((data) => {
            setUserState({...data.data});
        })
        .catch(err => {
            console.log(err)
        });
    }, []);

    return (
        <div>
            <Grid columns={2} stackable>
            <Grid.Column width={4}>
                {userState ? <UserInfo userState={userState} /> : null }
            </Grid.Column>
            <Grid.Column width={12}>
                {userState ? <UserEvents events={userState.createdEvents} /> : null }
            </Grid.Column>
            </Grid>
        </div>
        )
}

export default UserProfile;