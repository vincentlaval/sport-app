import React, { useState, useEffect } from 'react';
import axios from 'axios';

function Participate(props) {
  const [user, setUser] = useState('');
  const name = props.name;
  const description = props.description;
  const id = props.id;

  useEffect(() => {
    axios
      .post(
        'http://sportapp.formationfullstack.fr/api/userid',
        {
          headers: { 'Content-type': 'application/json', crossDimain: true }
        },
        { withCredentials: true }
      )
      .then(data => setUser(data.data))
      .catch(err => {
        props.setUserState(false);
      });
  });

  const handleParticipate = e => {
    const participants = user;
    console.log(participants + ' - ' + name + ' - ' + description);
    axios
      .put(`http://sportapp.formationfullstack.fr/api/events/${id}`, {
        headers: { 'Content-type': 'application/json' },
        data: {
          participants,
          name,
          description
        }
      })
      .then(data => console.log(data))
      .then(() => (document.location.href = window.location.href));
  };

  return <button onClick={handleParticipate}>Participate</button>;
}

export default Participate;
