import React, { useEffect } from 'react';
import axios from 'axios';

function SignOut(props) {
  useEffect(() => {
    axios
      .post(
        'http://sportapp.formationfullstack.fr/api/signout',
        {
          headers: { 'Content-type': 'application/json', crossDimain: true }
        },
        { withCredentials: true }
      )
      .then(data => {
        props.setUserState(false);
      })
      .catch(err => {
        props.setUserState(false);
      });
  }, []);

  document.body.style.backgroundImage =
    "url('http://sportapp.formationfullstack.fr/img/fitness.jpg')";
  document.body.style.backgroundSize = 'cover';

  return <h1>See you soon</h1>;
}
export default SignOut;
