import React, { useState, useEffect } from 'react';
import ReactMapGL, { Marker, Popup } from 'react-map-gl';
import { Button, Menu, Icon } from 'semantic-ui-react';
import { withRouter, NavLink } from 'react-router-dom';
import axios from 'axios';

function Mapcomp(props) {
  const [viewport, setViewport] = useState({
    latitude: 43.6043,
    longitude: 1.4437,
    zoom: 11,
    bearing: 0,
    pitch: 0
  });

  const MAPBOX_TOKEN =
    'pk.eyJ1IjoidmluY2UzMzEiLCJhIjoiY2s1YmRzbzNkMGJodzNsbzRudHAzYTk3NSJ9.rmP3XL-XRlvA1hw7fvkNSQ';

  const sendSelectedEvent = event => {
    props.setMyEvent(event);
  };

  const handleParticipate = e => {
    const participants = props.user;
    const name = props.marker.name;
    const description = props.marker.description;
    axios
      .put(`http://sportapp.formationfullstack.fr/api/events/${props.marker._id}`, {
        headers: { 'Content-type': 'application/json' },
        data: {
          participants,
          name,
          description
        }
      })
      .then(data => console.log(data));
  };

  return (
    <div>
      {props.events ? (
        <ReactMapGL
          {...viewport}
          width="60vw"
          height="60vh"
          mapboxApiAccessToken={MAPBOX_TOKEN}
          mapStyle="mapbox://styles/vince331/ck5in4zak16i71imuq9ieyzhq"
          onViewportChange={viewport => {
            setViewport(viewport);
          }}
        >
          {(props.sport
            ? props.events.filter(event => event.sport == props.sport)
            : props.events
          ).map(event => (
            <Marker
              key={event._id}
              latitude={event.location.coordinates[1]}
              longitude={event.location.coordinates[0]}
            >
              <Icon
                name="hand point down"
                size="large"
                color="teal"
                onClick={e => {
                  e.preventDefault();
                  sendSelectedEvent(event);
                }}
              />
            </Marker>
          ))}

          {props.marker ? (
            <Popup
              latitude={props.marker.location.coordinates[1]}
              longitude={props.marker.location.coordinates[0]}
              onClose={() => {
                sendSelectedEvent(null);
              }}
              closeOnClick={false}
            >
              <div>
                <h4>{props.marker.name}</h4>
                <p>{props.marker.description}</p>
                <Menu.Item
                  as={NavLink}
                  to={`/event/${props.marker._id}`}
                  name="details"
                />
              </div>
            </Popup>
          ) : null}
        </ReactMapGL>
      ) : null}
    </div>
  );
}

export default withRouter(Mapcomp);
