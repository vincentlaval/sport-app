import React from 'react';
import {Button, Form} from 'semantic-ui-react';

function SettingsForm (props) {

    return (
        <Form className="attached fluid segment" size="large">
            <Form.Field>
                <label>First name</label>
                <Form.Input icon="user" iconPosition="left" placeholder="First Name" type="text" name="fname" value={props.fname} onChange={props.handleInput} />
            </Form.Field>
            <Form.Field>
                <label>Last name</label>
                <Form.Input icon="user" iconPosition="left" placeholder="Last Name" type="text" name="lname" value={props.lname} onChange={props.handleInput} />
            </Form.Field>
            <Form.Field required>
                <label>Password</label>
                <Form.Input icon="lock" iconPosition="left"  placeholder="Password" type="password" name="password" value={props.password} onChange={props.handleInput} />
            </Form.Field>
            <Button type="submit" onClick={props.handleSubmit}>Submit</Button>
        </Form>
    )
}

export default SettingsForm;