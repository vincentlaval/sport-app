import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Input, Header, Grid, Form } from 'semantic-ui-react';
import SettingsForm from './SettingsForm';

function EditProfile(props) {
  const [user, setUser] = useState({});
  const [userId, setUserId] = useState('');
  useEffect(() => {
    axios
      .post(
        'http://sportapp.formationfullstack.fr/api/profile',
        {
          headers: { 'Content-type': 'application/json', crossDimain: true }
        },
        { withCredentials: true }
      )
      .then(data => {
        setUserId(data.data._id);
        props.setUserState(true);
      })
      .catch(err => {
        props.history.push('/login');
      });
  }, []);

  const handleInput = e => {
    setUser({ ...user, [e.target.name]: e.target.value });
  };

  const handleSubmit = e => {
    e.preventDefault();
    console.log(userId);
    axios
      .put(`http://sportapp.formationfullstack.fr/api/users/${userId}`, {
        headers: { 'Content-Type': 'application/json' },
        data: { ...user }
      })
      .then(data => console.log(data))
      .catch(err => console.log(err));
  };

  return (
    <Grid centered>
      <Grid.Column width={8}>
        <Header as="h1">Settings</Header>
        <SettingsForm handleInput={handleInput} handleSubmit={handleSubmit} />
      </Grid.Column>
    </Grid>
  );
}

export default EditProfile;
