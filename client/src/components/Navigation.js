import React from 'react';
import { Link } from 'react-router-dom';

function NavBar() {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  return (
    <AppBar position="relative">
      <Toolbar>
        <Button onClick={handleClick}>Menu</Button>
        <Menu
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={handleClose}
        >
          <Link to="/register">
            <MenuItem onClick={handleClose}>Register</MenuItem>
          </Link>
          <Link to="/login">
            <MenuItem onClick={handleClose}>Login</MenuItem>
          </Link>
          <Link to="/events">
            <MenuItem onClick={handleClose}>Events</MenuItem>
          </Link>
          <Link to="/profile">
            <MenuItem onClick={handleClose}>Profile</MenuItem>
          </Link>
          <Link to="/editProfile">
            <MenuItem onClick={handleClose}>Edit Profile</MenuItem>
          </Link>
        </Menu>
        <Typography variant="h6" align="center">
          <Link to="/home">Sports App</Link>
        </Typography>
      </Toolbar>
    </AppBar>
  );
}

export default NavBar;
