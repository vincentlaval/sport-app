import React, { useState, useEffect } from 'react';
import MapGL, { GeolocateControl, Marker } from 'react-map-gl';
import 'mapbox-gl/dist/mapbox-gl.css';
import { Icon, Popup } from 'semantic-ui-react';

const TOKEN =
  'pk.eyJ1IjoidmluY2UzMzEiLCJhIjoiY2s1YmRzbzNkMGJodzNsbzRudHAzYTk3NSJ9.rmP3XL-XRlvA1hw7fvkNSQ';

const geolocateStyle = {
  float: 'left',
  margin: '20px',
  padding: '10px'
};

const Map = props => {
  const [viewport, setViewPort] = useState({
    width: '100%',
    height: 300,
    latitude: 43.6043,
    longitude: 1.4437,
    zoom: 11
  });

  const _onViewportChange = viewport => setViewPort({ ...viewport });

  return (
    <div style={{ margin: '0 auto' }}>
      <MapGL
        {...viewport}
        mapboxApiAccessToken={TOKEN}
        mapStyle="mapbox://styles/vince331/ck5in4zak16i71imuq9ieyzhq"
        onViewportChange={_onViewportChange}
        onClick={event => props.getAddress(event)}
      >
        {props.coordinates.length !== 0 ? (
          <Marker
            latitude={props.coordinates[1]}
            longitude={props.coordinates[0]}
          >
            <Popup
              trigger={
                <Icon name="arrows alternate" size="small" color="blue" />
              }
            />
          </Marker>
        ) : null}

        <GeolocateControl
          style={geolocateStyle}
          positionOptions={{ enableHighAccuracy: true }}
          trackUserLocation={true}
        />
      </MapGL>
    </div>
  );
};

export default Map;
