import React, { useEffect, useState } from 'react';
import { Item, Icon, Segment, Header,Label } from 'semantic-ui-react';
import {Link} from 'react-router-dom'
import Axios from 'axios';

function Friends(props) {
    
    const [userState, setUserState] = useState([]);
    useEffect(() => {
        Axios.post(`http://sportapp.formationfullstack.fr/api/friends`, {
          
        }, {withCredentials: true})
        .then((data) => {
            console.log(data)
            setUserState({...data.data});
        })
        .catch(err => {
            console.log(err)
        });
    }, []);
    console.log(userState)

    document.body.style.backgroundImage="url('http://sportapp.formationfullstack.fr/img/runner.jpg')"
  document.body.style.backgroundSize="cover"
    return (
      <Segment raised>
        <Item.Group divided>
          {userState.length == 0 ? (
            <Header as="h2">No Friends!</Header>
          ) : (
            userState.friends.map(e => (
              <Item.Group key={e._id} link>
                <Item>
                  <Item.Image size='tiny' src='https://react.semantic-ui.com/images/avatar/large/molly.png' />

                  <Item.Content>
                    <Item.Header>{e.fname}</Item.Header>
                    <Item.Description>{e.description}</Item.Description>
                    <Link to={`/user/${e._id}`}>Go to profile</Link>
                  </Item.Content>
                </Item>
              </Item.Group>
            ))
          )}
        </Item.Group>
      </Segment>
    );
}

export default Friends;