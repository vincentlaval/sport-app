import React from 'react';
import { Dropdown } from 'semantic-ui-react';

const sportOptions = [
  {
    key: 'Foot',
    text: 'Foot',
    value: 'Foot'
  },
  {
    key: 'Basket',
    text: 'Basket',
    value: 'Basket'
  },
  {
    key: 'Tennis',
    text: 'Tennis',
    value: 'Tennis'
  },
  {
    key: 'Golf',
    text: 'Golf',
    value: 'Golf'
  },
  {
    key: 'Volley',
    text: 'Volley',
    value: 'Volley'
  },
  {
    key: 'Running',
    text: 'Running',
    value: 'Running'
  }
];

function SportSelection(props) {
  const sportValue = (e, data) => {
    sendData(data.value);
  };
  const sendData = sport => {
    props.sportSelection(sport);
  };
  return (
    <Dropdown
      placeholder="Select Sport"
      search
      selection
      options={sportOptions}
      onChange={sportValue}
    />
  );
}

export default SportSelection;
