import React, { useState } from 'react';
import { Form, Button, Image, Modal, Icon } from 'semantic-ui-react';
import axios from 'axios';
import './ProfileEvents.css';
import sportImage from './sport.jpg';
import Map from '../Map';

function ModalExampleDimmer(props) {
  const [state, setState] = useState({ open: false });
  const [form, setForm] = useState({});
  const [location, setLocation] = useState({
    type: 'Point',
    coordinates: props.event.location.coordinates
  });
  // méthode qui envoi l'event mis à jour au parent
  // regarder dans le .then data => console.log => la date renvoyée n'est pas à jour...
  const sendUpdatedEvent = event => {
    props.majEvents(event);
  };
  const id = props.event._id;

  const { open, dimmer } = state;

  const show = dimmer => () => {
    setState({ dimmer, open: true });
  };

  const close = () => {
    setState({ open: false });
  };

  const sportOptions = [
    {
      key: 'Foot',
      text: 'Foot',
      value: 'Foot'
    },
    {
      key: 'Basket',
      text: 'Basket',
      value: 'Basket'
    },
    {
      key: 'Tennis',
      text: 'Tennis',
      value: 'Tennis'
    },
    {
      key: 'Golf',
      text: 'Golf',
      value: 'Golf'
    },
    {
      key: 'Volley',
      text: 'Volley',
      value: 'Volley'
    },
    {
      key: 'Running',
      text: 'Running',
      value: 'Running'
    }
  ];
  const sportValue = (e, data) => {
    setForm({ ...form, sport: data.value });
  };

  function getAddress(event) {
    setLocation({ type: 'Point', coordinates: event.lngLat });
  }

  const handleChange = e => {
    setForm({ ...form, [e.target.name]: e.target.value });
  };

  const handleSubmit = e => {
    axios
      .put(`http://sportapp.formationfullstack.fr/api/events/${id}`, {
        headers: { 'Content-Type': 'application/json' },
        data: { ...form, location }
      })
      .then(data => sendUpdatedEvent(data.data), close())
      .catch(err => console.log(err));
  };

  return (
    <div>
      <Icon
        onClick={show(true)}
        className="cursorGrab"
        name="edit"
        color="blue"
      />

      <Modal dimmer={dimmer} open={open} onClose={close}>
        <Modal.Header>Edit My Event: {props.event.name}</Modal.Header>
        <Modal.Content image>
          <Image wrapped size="medium" src={sportImage} />
          <Modal.Description>
            <Form>
              <Form.Group>
                <Form.Field width="7">
                  <label>Name</label>
                  <input
                    placeholder="name"
                    name="name"
                    value={form.value}
                    onChange={handleChange}
                  />
                </Form.Field>
                <Form.Field width="11">
                  <label>Description</label>
                  <input
                    placeholder="description"
                    name="description"
                    value={form.value}
                    onChange={handleChange}
                  />
                </Form.Field>
                <Form.Select
                  fluid
                  label="Sport"
                  placeholder="Sport"
                  options={sportOptions}
                  onChange={sportValue}
                />
              </Form.Group>
              <Form.Group>
                <Form.Field width="7">
                  <label>Date</label>
                  <Form.Input
                    id="datetime-local"
                    type="datetime-local"
                    name="date"
                    value={form.value}
                    onChange={handleChange}
                  />
                </Form.Field>
              </Form.Group>
              <Map getAddress={getAddress} coordinates={location.coordinates} />
            </Form>
          </Modal.Description>
        </Modal.Content>
        <Modal.Actions>
          <Button color="black" onClick={close}>
            Cancel
          </Button>
          <Button
            positive
            icon="checkmark"
            labelPosition="right"
            content="Edit"
            onClick={handleSubmit}
          />
        </Modal.Actions>
      </Modal>
    </div>
  );
}

export default ModalExampleDimmer;
