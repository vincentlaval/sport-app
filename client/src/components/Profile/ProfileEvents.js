import React, { useState, useEffect } from 'react';
import Modale from './Modale';
import './ProfileEvents.css';

import { Icon, Item, Label, Input, Segment, Header } from 'semantic-ui-react';
import axios from 'axios';

function ProfileEvents(props) {
  const [searchTerm, setSearchTerm] = useState('');
  const [events, setEvents] = useState(props.events);

  // methode passée à l'enfant pour récupérer son updated event
  // on itère sur la liste des events présents, on va retrouver l'ancien event et le remplacer par
  // l'updated event

  const majEvents = event => {
    let newEvents = events.filter(element => element._id !== event._id);
    newEvents.unshift(event);
    setEvents(newEvents);
  };

  function handleInput(e) {
    setSearchTerm(e.target.value);
  }

  const handleDelete = event => {
    console.log('event selectionné :', event.name, 'id: ', event._id);
    let id = event._id;
    axios
      .delete(`http://sportapp.formationfullstack.fr/api/events/${id}`)
      .then(data => {
        setEvents(events.filter(event => event._id !== id));
      })
      .catch(err => {
        console.log(err);
      });
  };

  useEffect(() => {
    if (searchTerm != '') {
      const results = props.events.filter(event => {
        return event.sport.toLowerCase().includes(searchTerm.toLowerCase());
      });
      setEvents(results);
    } else {
      setEvents(props.events);
    }
  }, [searchTerm]);
  return (
    <Segment raised>
      <Input
        icon={<Icon name="search" inverted circular link />}
        onChange={handleInput}
        placeholder="Search..."
      />
      <Item.Group divided>
        {events.length == 0 ? (
          <Header as="h2">No events!</Header>
        ) : (
          events.map(e => (
            <Item key={e._id}>
              <Icon name="basketball ball" size="huge" />
              <Item.Content>
                <Item.Header as="a">{e.name}</Item.Header>
                <Item.Meta>
                  <span className="cinema">{e.sport}</span>
                </Item.Meta>
                <Item.Description>{e.description}</Item.Description>
                <Item.Extra>
                  <Label>
                    <Icon name="users" />
                    {e.participants.length}
                  </Label>
                </Item.Extra>
              </Item.Content>

              <Modale event={e} majEvents={majEvents} />
              <Icon
                className="cursorGrab"
                onClick={() => handleDelete(e)}
                name="close"
                color="red"
              />
            </Item>
          ))
        )}
      </Item.Group>
    </Segment>
  );
}

export default ProfileEvents;
