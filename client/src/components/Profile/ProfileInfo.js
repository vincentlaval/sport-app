import React from 'react';
import {Card, Icon, Image} from 'semantic-ui-react'

function ProfileInfo(props) {
  console.log(props)
  const {fname, lname, email, friends, description, createdAt} = props.profileState;
  return (
    <Card color="teal">
      <Image src='https://react.semantic-ui.com/images/avatar/large/molly.png' wrapped ui={false} />
      <Card.Content>
      <Card.Header>{fname} {lname}</Card.Header>
        <Card.Meta>
          <div><Icon name="mail"/> {email}</div>
          <span className='date'><Icon name="wait"/>Joined in {new Date(createdAt).getFullYear()}</span>
        </Card.Meta>
        <Card.Description>
          {description ? description : <p>No description</p>}
        </Card.Description>
      </Card.Content>
      <Card.Content extra>
        <a>
          <Icon name='user' />
          {friends.length ? friends.length : "No friends"}
        </a>
      </Card.Content>
    </Card>
  )
}

export default ProfileInfo;