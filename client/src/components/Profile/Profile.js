import React, { useEffect, useState } from 'react';
import axios from 'axios';
import ProfileInfo from './ProfileInfo';
import ProfileEvents from './ProfileEvents';
import { Grid, Icon } from 'semantic-ui-react';
import { Link } from 'react-router-dom';

function Profile(props) {
  const [profileState, setProfileState] = useState('');
  const [isVisible, setIsVisible] = useState(false);

  useEffect(() => {
    axios
      .post(
        'http://sportapp.formationfullstack.fr/api/profile',
        {
          headers: {
            'Content-type': 'application/json',
            crossDimain: true
          }
        },
        { withCredentials: true }
      )
      .then(data => {
        console.log();
        setProfileState({ ...data.data });
        setIsVisible(true);
        props.setUserState(true);
      })
      .catch(err => {
        props.setUserState(false);
        props.history.push('/login');
      });
  }, []);
  document.body.style.backgroundImage =
    "url('http://sportapp.formationfullstack.fr/img/golf.jpg')";
  document.body.style.backgroundSize = 'cover';

  return (
    <div>
      {isVisible ? (
        <Grid columns={2} stackable>
          <Grid.Column width={4}>
            <ProfileInfo profileState={profileState} />
            <Link to="/settings">
              <Icon name="setting" />
              Settings
            </Link>
          </Grid.Column>
          <Grid.Column width={12}>
            <ProfileEvents events={profileState.createdEvents} />
          </Grid.Column>
        </Grid>
      ) : null}
    </div>
  );
}

export default Profile;
