import React, { useState, useEffect } from 'react';
import axios from 'axios';

function Departicipate(props) {
  const [user, setUser] = useState('');
  const name = props.name;
  const description = props.description;
  const parts = props.participants;
  const id = props.id;
  const departicipate = useState('departicipate');

  useEffect(() => {
    axios
      .post(
        'http://sportapp.formationfullstack.fr/api/userid',
        {
          headers: { 'Content-type': 'application/json', crossDimain: true }
        },
        { withCredentials: true }
      )
      .then(data => setUser(data.data))
      .catch(err => {
        props.setUserState(false);
      });
  });

  const handleDeparticipate = e => {
    console.log(parts);
    const participants = parts
      .filter(participant => participant._id !== user)
      .map(participant => participant._id);
    console.log(participants);
    axios
      .put(`http://sportapp.formationfullstack.fr/api/events/${id}`, {
        headers: { 'Content-type': 'application/json' },
        data: {
          participants,
          name,
          description,
          departicipate
        }
      })
      .then(data => console.log(data))
      .then(() => (document.location.href = window.location.href));
  };

  return <button onClick={handleDeparticipate}>Unsubscribe</button>;
}

export default Departicipate;
