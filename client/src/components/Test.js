import React, { useEffect, useState } from 'react';
import axios from 'axios';

function Test(props) {
  console.log(props);
  useEffect(() => {
    axios
      .post(
        'http://sportapp.formationfullstack.fr/api/auth',
        {
          headers: { 'Content-type': 'application/json', crossDimain: true }
        },
        { withCredentials: true }
      )
      .then(data => {
        props.setUserState(!props.userState);
        props.history.push('/profile');
      });
  });

  return <h1>EVENTS</h1>;
}

export default Test;
