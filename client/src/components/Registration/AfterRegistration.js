import React from 'react';
import {Message} from 'semantic-ui-react'

function RegistrationForm(props) {
    const header = `Activation link was sent to ${props.email}`
    return (
        <Message
            success
            size='huge'
            icon='mail'
            header={header}
            content='In order to finsih registration you have to activate your account:)'
        />
    );
}

export default RegistrationForm;