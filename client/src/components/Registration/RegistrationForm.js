import React from 'react';
import {Grid, Form, Message, Button, Checkbox} from 'semantic-ui-react';

function RegistrationForm(props) {
    const fnameError =  props.isValid.fname || (props.fname.length === 0)? null: { content: 'Your name can\'t contain numbers or signs', pointing: 'below' };
    const lnameError =  props.isValid.lname || (props.lname.length === 0)? null: { content: 'Your last name can\'t contain numbers or signs', pointing: 'below' };
    const emailError =  props.isValid.email || (props.email.length === 0)? null: { content: 'Your email is incorrect', pointing: 'below' };
    const passwordError =  props.isValid.password || (props.password.length === 0)? null: { content: 'Password must be at least 8 characters and include at least one number, one capital letter and one sign', pointing: 'below' };
    // const checkBoxError = !props.checkBoxDisplay ? null : { content: 'You must agree with our terms before continue', pointing: 'right'};
    return (
        <Grid centered>
            <Grid.Column width={8}>
            <Message
                attached
                size="big"
                header='Sign Up'
                content='Fill out the form below to sign-up for a new account'
                />
                <Form className="attached fluid segment" size="large">
                    <Form.Field required>
                        <label>First name</label>
                        <Form.Input error={fnameError} icon="user" iconPosition="left" placeholder="First Name" type="text" name="fname" value={props.fname} onChange={props.handleInput} />
                    </Form.Field>
                    <Form.Field required>
                        <label>Last name</label>
                        <Form.Input error={lnameError} icon="user" iconPosition="left" placeholder="Last Name" type="text" name="lname" value={props.lname} onChange={props.handleInput} />
                    </Form.Field>
                    <Form.Field required>
                        <label>Email</label>
                        <Form.Input error={emailError} icon="at" iconPosition="left"  placeholder="Email" type="email" name="email" value={props.email}  onChange={props.handleInput} />
                    </Form.Field>
                    <Form.Field required>
                        <label>Password</label>
                        <Form.Input error={passwordError} icon="lock" iconPosition="left"  placeholder="Password" type="password" name="password" value={props.password} onChange={props.handleInput} />
                    </Form.Field>
                    <Form.Checkbox required error={props.checkBoxDisplay} label='I agree to the Terms and Conditions' checked={props.toggle} onChange={props.handleToggle}/>
                    <Button type="submit" onClick={props.handleSubmit}>Submit</Button>
                </Form>
            </Grid.Column>
        </Grid>
    );
}

export default RegistrationForm;