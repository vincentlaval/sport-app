import React, { useState, useEffect } from 'react';
import axios from 'axios';
import RegistrationForm from './RegistrationForm';
import AfterRegistration from './AfterRegistration';

function Registration(props) {
  const [form, setForm] = useState({
    fname: '',
    lname: '',
    email: '',
    password: ''
  });
  const [isValid, setIsValid] = useState({
    fname: '',
    lname: '',
    email: '',
    password: '',
    checked: ''
  });
  const [checkBoxDisplay, setCheckBoxDisplay] = useState(false);
  const [isSend, setIsSend] = useState(false);
  useEffect(() => {
    axios
      .post(
        'http://sportapp.formationfullstack.fr/api/auth',
        {
          headers: { 'Content-type': 'application/json', crossDimain: true }
        },
        { withCredentials: true }
      )
      .then(data => {
        props.setUserState(true);
      })
      .catch(err => {
        props.setUserState(false);
      });
  }, []);

  function handleToggle() {
    if (isValid.checked == '') setIsValid({ ...isValid, checked: true });
    else setIsValid({ ...isValid, checked: !isValid.checked });
  }

  function handleInput(e) {
    if (e.target.name == 'fname' || e.target.name == 'lname') {
      const re = /^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/;
      setIsValid({ ...isValid, [e.target.name]: re.test(e.target.value) });
    } else if (e.target.name == 'email') {
      const re = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2}|com|org|net|gov|mil|biz|info|mobi|name|aero|jobs|museum)\b/;
      setIsValid({ ...isValid, [e.target.name]: re.test(e.target.value) });
    } else {
      const re = /(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/;
      setIsValid({ ...isValid, [e.target.name]: re.test(e.target.value) });
    }
    setForm({ ...form, [e.target.name]: e.target.value });
  }

  function handleSubmit(e) {
    if (!isValid.checked) setCheckBoxDisplay(true);
    if (Object.values(isValid).every(i => i)) {
      setIsSend(true);
      axios
        .post('http://sportapp.formationfullstack.fr/api/users', {
          headers: { 'Content-type': 'application/json' },
          data: { ...form }
        })
        .then(data => {
          setTimeout(() => {
            props.history.push('/home');
          }, 5000);
        })
        .catch(err => console.log(err));
    }
  }
  return (
    <div>
      {isSend ? (
        <AfterRegistration email={form.email} />
      ) : (
        <RegistrationForm
          checkBoxDisplay={checkBoxDisplay}
          handleSubmit={handleSubmit}
          handleToggle={handleToggle}
          handleInput={handleInput}
          {...form}
          isValid={{ ...isValid }}
        />
      )}
    </div>
  );
}

export default Registration;
