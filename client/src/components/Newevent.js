import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Button, Form, Grid, Message } from 'semantic-ui-react';
import Map from './Map';

function NewEvent(props) {
  const [form, setForm] = useState({
    name: '',
    sport: '',
    description: '',
    date: ''
  });
  const [location, setLocation] = useState({ type: '', coordinates: [] });
  const [creator, setCreator] = useState('');

  function getAddress(event) {
    setLocation({ type: 'Point', coordinates: event.lngLat });
  }

  useEffect(() => {
    axios
      .post(
        'http://sportapp.formationfullstack.fr/api/userid',
        {
          headers: {
            'Content-type': 'application/json',
            crossDimain: true
          }
        },
        { withCredentials: true }
      )
      .then(data => setCreator(data.data))
      .catch(err => {
        props.setUserState(false);
      });
  }, []);

  function handleInput(e) {
    setForm({ ...form, [e.target.name]: e.target.value });
  }

  function handleSubmit(e) {
    e.preventDefault();
    axios
      .post('http://sportapp.formationfullstack.fr/api/events', {
        headers: { 'Content-type': 'application/json' },
        data: {
          ...form,
          creator,
          location
        }
      })
      .then(data => props.history.push('/events'))
      .catch(err => console.log(err));
  }
  document.body.style.backgroundImage =
    "url('http://sportapp.formationfullstack.fr/img/piste.jpg')";
  document.body.style.backgroundSize = 'cover';

  return (
    <Grid centered>
      <Grid.Column width={8}>
        <Message attached size="big" header="New Event" />
        <Form className="attached fluid segment" size="large">
          <Form.Field>
            <label>Event name</label>
            <Form.Input
              icon="basketball ball"
              iconPosition="left"
              placeholder="your event"
              type="text"
              name="name"
              value={form.name}
              onChange={handleInput}
            />
          </Form.Field>
          <Form.Field>
            <label>Sport</label>
            <Form.Input
              icon="basketball ball"
              iconPosition="left"
              placeholder=""
              type="text"
              name="sport"
              value={form.sport}
              onChange={handleInput}
            />
          </Form.Field>
          <Form.Field>
            <label>Description</label>
            <Form.Input
              icon="edit"
              iconPosition="left"
              placeholder=""
              type="text"
              name="description"
              value={form.description}
              onChange={handleInput}
            />
          </Form.Field>
          <Form.Field>
            <label>When</label>
            <Form.Input
              id="datetime-local"
              type="datetime-local"
              name="date"
              value={form.date}
              onChange={handleInput}
            />
          </Form.Field>
          <Form.Field>
            <label>Where</label>
          </Form.Field>
          <Map getAddress={getAddress} coordinates={location.coordinates} />
          <br></br>
          <Button type="submit" onClick={handleSubmit}>
            Add Event
          </Button>
        </Form>
      </Grid.Column>
    </Grid>
  );
}

export default NewEvent;
