import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Button, Form, Grid, Message } from 'semantic-ui-react';
import { Link } from 'react-router-dom';

function Login(props) {
  const [form, setForm] = useState({ email: '', password: '' });
  const [error, setError] = useState(false);

  function handleInput(e) {
    setForm({ ...form, [e.target.name]: e.target.value });
  }

  function handleSubmit(e) {
    if (form.email.length !== 0 && form.password.length !== 0) {
      axios
        .post(
          'http://sportapp.formationfullstack.fr/api/login',
          {
            headers: {
              'Content-type': 'application/json',
              crossDimain: true
            },
            data: { ...form }
          },
          { withCredentials: true }
        )
        .then(data => {
          props.setUserState(true);
          props.history.push('/profile');
        })
        .catch(err => {
          // console.log(err)
          setError(true);
        });
    }
  }
  document.body.style.backgroundImage =
    "url('http://sportapp.formationfullstack.fr/img/login.jpg')";
  document.body.style.backgroundSize = 'cover';

  return (
    <Grid centered>
      <Grid.Column width={8}>
        <Message
          attached
          size="big"
          header="Sign In"
          content="Fill out the form below to sign-in"
        />
        <Form className="attached fluid segment" size="large">
          {error ? (
            <Message
              attached
              color="red"
              size="small"
              header="Email or Password are incorrect"
              content="Please try again"
            />
          ) : null}
          <Form.Field>
            <label>Email</label>
            <Form.Input
              placeholder="Email"
              icon="at"
              iconPosition="left"
              type="email"
              name="email"
              onChange={handleInput}
            />
          </Form.Field>
          <Form.Field>
            <label>Password</label>
            <Form.Input
              placeholder="Password"
              icon="lock"
              iconPosition="left"
              type="password"
              name="password"
              onChange={handleInput}
            />
          </Form.Field>
          <Button type="submit" onClick={handleSubmit}>
            Submit
          </Button>
          <div>
            <Link to="/password">Forgot password</Link>
          </div>
        </Form>
      </Grid.Column>
    </Grid>
  );
}

export default Login;
