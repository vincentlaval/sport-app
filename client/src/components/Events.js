import React, { useState, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import axios from 'axios';
import Mapcomp from './Mapcomp';
import SportSelection from './SportSelection';
import { Header, Grid } from 'semantic-ui-react';

function Events(props) {
  const [events, setEvents] = useState('');
  const [sport, setSport] = useState('');
  const [selectedEvent, setSelectedEvent] = useState(null);

  useEffect(() => {
    axios
      .post(
        'http://sportapp.formationfullstack.fr/api/auth',
        {
          headers: {
            'Content-type': 'application/json',
            crossDimain: true
          }
        },
        { withCredentials: true }
      )
      .then(data => {
        props.setUserState(true);
      })
      .catch(err => {
        props.setUserState(false);
        props.history.push('/login');
      });
    axios
      .get('http://sportapp.formationfullstack.fr/api/events')
      .then(data => {
        setEvents({ ...events, data });
      })
      .catch(err => {
        console.log(err);
      });
  }, []);

  const sportSelection = sport => {
    setSport(sport);
    setSelectedEvent(null);
  };

  const setMyEvent = event => {
    setSelectedEvent(event);
  };

  document.body.style.backgroundImage =
    "url('http://sportapp.formationfullstack.fr/img/running.jpg')";
  document.body.style.backgroundSize = 'cover';

  return (
    <div>
      {props.userState && events ? (
        <div>
          <Grid>
            <Grid.Column width={3}>
              <SportSelection sportSelection={sportSelection} />
            </Grid.Column>
            <Grid.Column width={13}>
              <Mapcomp
                events={events.data.data}
                sport={sport}
                setMyEvent={setMyEvent}
                marker={selectedEvent}
              />
            </Grid.Column>
          </Grid>
        </div>
      ) : null}
    </div>
  );
}
export default withRouter(Events);
