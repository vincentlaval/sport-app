import React from 'react';
import {Button, Form, Grid, Message} from 'semantic-ui-react';

function ChangePasswordForm(props) {
    return(
        <Grid centered >
            <Grid.Column width={8}>
            <Message
                attached
                size="big"
                header='New password'
                content='Type new password'
                />
                <Form className="attached fluid segment" size="large">
                    <Form.Field>
                        <label>Password</label>
                        <Form.Input placeholder="Password" value={props.password1} icon="at" iconPosition="left" type="password" name="password1" onChange={props.handleInput} />
                    </Form.Field>
                    <Form.Field>
                        <label>Confirm Password</label>
                        <Form.Input placeholder="Password" value={props.password2} icon="at" iconPosition="left" type="password" name="password2" onChange={props.handleInput} />
                    </Form.Field>
                    <Button type="submit" onClick={props.handleSubmit}>Submit</Button>
                </Form>
            </Grid.Column>
        </Grid>
    )
}

export default ChangePasswordForm;