import React from 'react';
import {Button, Form, Grid, Message} from 'semantic-ui-react';

function ForgotPasswordForm(props) {
    return(
        <Grid centered >
            <Grid.Column width={8}>
            <Message
                attached
                size="big"
                header='Forgot Password'
                content='Please provide your email and we will send you a link to change your password'
                />
                <Form className="attached fluid segment" size="large">
                    <Form.Field>
                        <label>Email</label>
                        <Form.Input placeholder="Email" value={props.email} icon="at" iconPosition="left" type="email" name="email" onChange={props.handleInput} />
                    </Form.Field>
                    <Button type="submit" onClick={props.handleSubmit}>Submit</Button>
                </Form>
            </Grid.Column>
        </Grid>
    )
}

export default ForgotPasswordForm;