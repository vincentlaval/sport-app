import React, { useState } from 'react';
import axios from 'axios';
import AfterSendingEmail from './AfterSendingEmail';
import ChangePasswordForm from './ChangePasswordForm';

function ChangePassword(props) {
  const [password, setPassword] = useState({ password1: '', password2: '' });
  const [passwordIsSent, setPasswordIsSent] = useState(false);

  function handleInput(e) {
    setPassword({ ...password, [e.target.name]: e.target.value });
  }

  function handleSubmit(e) {
    setPasswordIsSent(true);
    console.log(props.match.params.id);
    axios
      .put(
        `http://sportapp.formationfullstack.fr/api/password?token=${props.match.params.id}`,
        {
          headers: { 'Content-type': 'application/json', crossDimain: true },
          data: { password: password.password1 }
        },
        { withCredentials: true }
      )
      .then(data => {
        setTimeout(() => props.history.push('/login'), 5000);
      });
  }
  console.log(password.password1);
  return (
    <div>
      {passwordIsSent ? (
        <AfterSendingEmail
          email={'Your password has been changed'}
          text={
            "Your password has been changed you'll be redirected to the login page in 5 sec"
          }
        />
      ) : (
        <ChangePasswordForm
          password={password}
          handleInput={handleInput}
          handleSubmit={handleSubmit}
        />
      )}
    </div>
  );
}

export default ChangePassword;
