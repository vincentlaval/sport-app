import React from 'react';
import {Message} from 'semantic-ui-react'

function AfterSendingEmail(props) {
    const header = `Activation link was sent to ${props.email}`
    return (
        <Message
            success
            size='huge'
            icon='check'
            header={props.header}
            content={props.text}
        />
    );
}

export default AfterSendingEmail;