import React, { useState } from 'react';
import axios from 'axios';
import AfterSendingEmail from './AfterSendingEmail';
import ForgotPasswordForm from './ForgotPasswordForm';

function ForgotPassword(props) {
  const [email, setEmail] = useState('');
  const [emailIsSent, setEmailIsSet] = useState(false);

  function handleInput(e) {
    setEmail(e.target.value);
  }

  function handleSubmit(e) {
    if (email.length !== 0) {
      setEmailIsSet(true);
      axios
        .post(
          'http://sportapp.formationfullstack.fr/api/password',
          {
            headers: { 'Content-type': 'application/json', crossDimain: true },
            data: { email }
          },
          { withCredentials: true }
        )
        .then(data => {
          setTimeout(() => props.history.push('/home'), 5000);
        });
    }
  }

  return (
    <div>
      {emailIsSent ? (
        <AfterSendingEmail
          email={`Activation link was sent to ${email}`}
          text={'We have sent you an activation link'}
        />
      ) : (
        <ForgotPasswordForm
          email={email}
          handleInput={handleInput}
          handleSubmit={handleSubmit}
        />
      )}
    </div>
  );
}

export default ForgotPassword;
