import React, { useState } from 'react';
import Home from './components/Home';
import Registration from './components/Registration/Registration';
import Login from './components/Login';
import Events from './components/Events';
import Event from './components/Event/Event'
import Activate from './components/Activation/Activate';
import Profile from './components/Profile/Profile';
import SignOut from './components/SignOut';
import ForgotPassword from './components/Password/ForgotPassword'
import ChangePassword from './components/Password/ChangePassword';
import Settings from './components/SettingsPage/Settings';
import UserProfile from './components/UserProfile/UserProfile';
import Friends from './components/Friends/Friends';

import {
  BrowserRouter as Router,
  Route,
  NavLink,
  Switch
} from 'react-router-dom';
import { Menu, Container, Icon } from 'semantic-ui-react';
import NewEvent from './components/Newevent'

function App() {
  const [userState, setUserState] = useState(false);
  const [img, setImg] = useState('nuit')

  function renderAuth() {
    return userState ? (
      <Menu size="huge" color="teal">
        <Menu.Item as={NavLink} to="/home" name="home">
          <Icon name="basketball ball" size="large" />
          Sport app
        </Menu.Item>
        <Menu.Menu position="right">
        <Menu.Item as={NavLink} to="/newevent" name="newevent"><Icon name="plus circle" />Event</Menu.Item>
        <Menu.Item as={NavLink} to="/friends" name="events">
            Friends
          </Menu.Item>
          <Menu.Item as={NavLink} to="/events" name="events">
            Events
          </Menu.Item>
          <Menu.Item as={NavLink} to="/profile" name="profile">
            Profile
          </Menu.Item>
          <Menu.Item as={NavLink} to="/signout" name="signout">
            Signout
          </Menu.Item>
        </Menu.Menu>
      </Menu>
    ) : (
      <Menu size="huge" color="teal" pointing secondary>
        <Menu.Item as={NavLink} to="/home" name="home">
          <Icon name="basketball ball" size="large" />
          Sport app
        </Menu.Item>
        <Menu.Menu position="right">
          <Menu.Item as={NavLink} to="/login" name="profile">
            <Icon name="sign-in" />
            Sign In
          </Menu.Item>
          <Menu.Item as={NavLink} to="/registration" name="profile">
            <Icon name="signup" />
            Sign Up
          </Menu.Item>
        </Menu.Menu>
      </Menu>
    );
  }
  return (
    <Router>
      {renderAuth()}
      <div style={{overflow: 'scroll', height: '90%', padding: '2rem'}}>
      <Container>
        <Switch>
          <Route
            exact
            path="/login"
            render={history => (
              <Login setImg={setImg}
                userState={userState}
                setUserState={setUserState}
                {...history}
              />
            )}
          />
          <Route
            exact
            path="/registration"
            render={history => (
              <Registration
                userState={userState}
                setUserState={setUserState}
                {...history}
              />
            )}
          />
          <Route
            exact
            path="/events"
            render={history => (
              <Events
                userState={userState}
                setUserState={setUserState}
                {...history}
              />
            )}
          />
          <Route
            path="/event/:id"
            render={history => (
              <Event
                userState={userState}
                setUserState={setUserState}
                {...history}
              />
            )}
          />
          <Route
            exact
            path="/profile"
            render={history => (
              <Profile
                userState={userState}
                setUserState={setUserState}
                {...history}
                setImg={setImg}
              />
            )}
          />
          <Route exec path="/settings"  render={ (history) => <Settings userState={userState} setUserState={setUserState} {...history}/>}/>
          <Route exact path="/activate/:id" component={Activate} />
          <Route exact path="/signout" render={(history) => <SignOut userState={userState} setUserState={setUserState} {...history} />} />
          <Route exact path="/password" render={(history) => <ForgotPassword {...history}/>} />
          <Route exact path="/recover-password/:id" render={(history) => <ChangePassword {...history}/>} />
          <Route exact path="/home" component={Home} />
          <Route exact path="/" component={Home} />
          <Route path="/newevent" render={(history) => <NewEvent userState={userState} setUserState={setUserState} {...history} />} />
          <Route exact path="/user/:id" render={(history) => <UserProfile userState={userState} setUserState={setUserState} {...history} />} />
          <Route exact path="/friends" render={(history) => <Friends userState={userState} setUserState={setUserState} {...history} />} />
        </Switch>
      </Container>
      </div>
    </Router>
  );
}

export default App;
