const Event = require('../model/eventModel');
const User = require('../model/userModel');

exports.getEvent = async (req, res, next) => {
  const event = await Event.findById(req.params.id)
    .populate('creator')
    .populate('participants');
  res.status(200).json(event);
};

exports.getAllEvents = async (req, res, next) => {
  const events = await Event.find({});
  res.status(200).json(events);
};

exports.createEvent = async (req, res, next) => {
  console.log(req.body.data);
  const event = await Event.create(req.body.data);
  const user = await User.findById(req.body.data.creator);
  user.createdEvents.push(event);
  await User.findByIdAndUpdate(user.id, user, {
    new: true
  });
  res.status(200).json(event);
};

exports.updateEvent = async (req, res, next) => {
  console.log(req.body)
  if (req.body.data.departicipate) {
    const { name, description, participants } = req.body.data;
    const event = await Event.findByIdAndUpdate(
    req.params.id,
    { name, description,  participants: participants },
    { new: true }
  );
  res.status(200).json(event);
  }
  else {
  const { name, description, participants } = req.body.data;
  const event = await Event.findByIdAndUpdate(
    req.params.id,
    { name, description, $addToSet: { participants: participants } },
    { new: true }
  );
  res.status(200).json(event);
  }

}

exports.updateEvent = async (req, res, next) => {
  if (req.body.data.departicipate) {
    const { name, description, participants } = req.body.data;
    const event = await Event.findByIdAndUpdate(
    req.params.id,
    { name, description,  participants: participants },
    { new: true }
  );
  res.status(200).json(event);
  } else if (req.body.data.participants) {
    const { name, description, participants } = req.body.data;
    const event = await Event.findByIdAndUpdate(
      req.params.id,
      { name, description, $addToSet: { participants: participants } },
      { new: true }
    );
    res.status(200).json(event);
  } else {
    const event = await Event.findByIdAndUpdate(req.params.id, req.body.data, {
      new: true
    });
    res.status(200).json(event);
  }
};

exports.deleteEvent = async (req, res, next) => {
  console.log(req.params.id);
  await Event.findByIdAndDelete(req.params.id);
  res.status(200).json({ status: 'success' });
};
