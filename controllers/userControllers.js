const User = require('../model/userModel');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const nodemailer = require('../utils/nodemailer');
const uuid = require('uuid');
require('dotenv').config({ path: '../config/.env' });

exports.getUser = async (req, res, next) => {
  if (req.params.id) {
    const user = await User.findById(req.params.id).populate('createdEvents');
    return res.status(200).json(user);
  } else {
    const user = await User.findById(req.body.decodedToken.id).populate(
      'createdEvents'
    );
    return res.status(200).json(user);
  }
};

exports.getAllUsers = async (req, res, next) => {
  const users = await User.find({});
  res.status(200).json(users);
};

exports.createUser = async (req, res, next) => {
  const user = await User.create(req.body.data);
  let token = jwt.sign(
    {
      email: user.email,
      fname: user.fname,
      lname: user.lname
    },
    process.env.SECRET
  );
  nodemailer(req.body.data.email, token, 'registration');
  res.status(201).json({ user: user, token: token });
};

// exports.updateUserDetailes = async (req, res, next) => {
//   if (req.body.data.password) {
//     const id = req.params.id ;
//     var user = await User.findByIdAndUpdate({_id: id}, {...req.body.data}, {
//       new: true
//     });
//   }
//   res.status(200).json(user);
// }

exports.addUser = async (req, res, next) => {
  await User.findByIdAndUpdate(
    { _id: req.body.decodedToken.id },
    { $addToSet: { friends: req.params.id } },
    { new: true }
  );
  res.sendStatus(200);
};

exports.getFriends = async (req, res, next) => {
  console.log(req.body.decodedToken);
  const user = await User.findById({ _id: req.body.decodedToken.id }).populate(
    'friends'
  );
  res.json(user);
};

exports.updateUser = async (req, res, next) => {
  const { password, fname, lname } = req.body.data;
  const { id } = req.params;
  const user = await User.findById({ _id: id });
  const match = await bcrypt.compare(password, user.password);
  if (match) {
    var updatedUser = await User.findByIdAndUpdate(
      { _id: id },
      { fname, lname },
      {
        new: true
      }
    );
  }
  // const password = req.body.data.password;
  // if (password) {
  //   const newPassword = bcrypt.hashSync(password, 10);
  //   var user = await User.findByIdAndUpdate({_id: req.body.decodedToken._id}, {password: newPassword}, {
  //     new: true
  //   });
  // }
  res.status(200).json(updatedUser);
};

exports.deleteUser = async (req, res, next) => {
  await User.findByIdAndDelete(req.params.id);
  res.status(200).json({ status: 'success' });
};

exports.onLogin = async (req, res, next) => {
  console.log(req.body);
  try {
    const user = await User.findOne({ email: req.body.data.email });
    console.log(user);

    if (!user || !user.isVerified)
      throw { error: 'Email or Password are incorrect.' };
    const match = await bcrypt.compare(req.body.data.password, user.password);
    if (match) {
      const userToken = await jwt.sign(
        {
          id: user.id,
          email: user.email,
          fname: user.fname,
          lname: user.lname
        },
        process.env.SECRET
      );
      return res
        .status(200)
        .set('Access-Control-Allow-Origin', 'http://localhost:3000')
        .cookie('token', userToken)
        .json({ status: 'success' });
    }
  } catch (err) {
    return res.status(400).json({ error: err.error });
  }
};

exports.activateUser = async (req, res, next) => {
  await User.findOneAndUpdate(
    { email: req.body.decodedToken.email },
    { isVerified: true },
    { new: true }
  );
  res.sendStatus(200);
};

exports.userSignout = async (req, res, next) => {
  res.clearCookie('token').sendStatus(200);
};

exports.forgotPassword = async (req, res, next) => {
  const user = await User.findOne({ email: req.body.data.email });
  const token = await jwt.sign(
    {
      id: user._id,
      email: user.email,
      fname: user.fname,
      lname: user.lname,
      psswID: uuid()
    },
    process.env.SECRET,
    { expiresIn: '30m' }
  );
  nodemailer(req.body.data.email, token, 'password');
  res.sendStatus(200);
};

exports.changePassword = async (req, res, next) => {
  await User.findOneAndUpdate(
    { email: req.body.decodedToken.email },
    { password: req.body.data.password },
    { new: true }
  );
  res.sendStatus(200);
};

exports.getUserid = async (req, res, next) => {
  res.send(req.body.decodedToken.id);
};
